import React, { Component } from "react";
import SignIn from "./SignIn";
import { Grid, Row, Col, Table } from "react-bootstrap";
import * as firebase from "firebase";
import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import {bsSizes} from "react-bootstrap/lib/utils/bootstrapUtils";
import {Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import ProjectView from "./ProjectView";


// const db = firebase.firestore();

let projectPath = '/home/projects/projectview/';
let createProjectPath = '/home/createproject';

let projectsArray = [[], []];
let userData;
let projects;

function getUserProjects() {

  // return new Promise( (resolve, reject) => {
  //   setTimeout(() => {
  //     db.collection("Users").doc(firebase.auth().currentUser.uid).get().then(function(doc) {
  //       if (doc.exists) {
  //         userData = doc.data();
  //
  //         for(let i = 0; i < 2; i++)
  //         {
  //           db.collection("Projects").doc(userData["projects"][i]).get().then(function(doc)
  //           {
  //             if (doc.exists) {
  //               projects = doc.data();
  //               projectsArray[i][0] = i.toString();
  //               projectsArray[i][1] = projects["name"];
  //               projectsArray[i][2] = projects["description"];
  //               console.log(projectsArray[i][1]);
  //             }
  //             resolve(projectsArray[i][1]);
  //           });
  //         }
  //       } else {
  //         console.log("No such document!");
  //         userData = {};
  //         resolve("Nasol")
  //       }
  //     })
  //   }, 2000);
  //
  // })

}

class Projects extends Component {

  constructor(props){
    super(props);
    this.state = {
      userProjects: ""
    }
  }

  componentDidMount() {
    // getUserProjects().then(res => {
    //   this.setState({userProjects: res})
    // })
  }

  render() {

    const {userProjects} = this.state;

    return (

        <div className="content">
          <Grid fluid>
            <Row>
              <Col md={12}>
                <Card
                    title="Personal Projects"
                    ctTableFullWidth
                    ctTableResponsive
                    content={
                      <Table striped hover>
                        <thead>
                        <tr>
                          {thArray.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                        </thead>
                        <tbody>
                        {projectsArray.map((prop, key) => {
                          return (
                              <tr key={key} onClick={() => {
                                window.location.href = projectPath + prop[1]
                                // console.log(prop);
                              }}>
                                {

                                  prop.map((prop, key) => {
                                    return <td key={key}>{prop}</td>;
                                    // console.log(prop);
                                  })
                                }

                              </tr>
                          );
                        })}
                        </tbody>
                        <Button style={{marginTop: 20, marginLeft: 20, _height: 30, _weigh: 40, bsSizes: 'large'}}
                                onClick={() => {
                                  window.location.href = createProjectPath
                                }}>
                          Create Project
                        </Button>
                      </Table>


                    }
                />
              </Col>

              <Col md={12}>
                <Card
                    title="Other Projects"
                    ctTableFullWidth
                    ctTableResponsive
                    content={
                      <Table hover>
                        <thead>
                        <tr>
                          {thArray.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                        </thead>
                        <tbody>
                        {projectsArray.map((prop, key) => {
                          return (
                              <tr key={key} onClick={() => {
                                window.location.href = projectPath + prop[1]
                              }}>
                                {prop.map((prop, key) => {
                                  return <td key={key}>{prop}</td>;
                                })}
                              </tr>
                          );
                        })}
                        </tbody>
                      </Table>
                    }
                />
              </Col>
            </Row>
          </Grid>
        </div>
    )}
}

export default Projects;
