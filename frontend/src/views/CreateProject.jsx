import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,
} from "react-bootstrap";

import SignIn from "./SignIn";
import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";


// import {FieldValueImpl as FieldValue} from "@firebase/firestore/dist/src/api/field_value";

// const db = firebase.firestore();
let uuidv4 = require('uuid/v4');
let uuid = uuidv4();

function getDescription () {
    return document.getElementById("exampleFormControlTextarea1").value;
}

function getName () {
    return document.getElementById("name").value;
}

function addProjectToDb(name, description) {
    //
    // db.collection('Projects').doc(uuid).set({
    //     name: name,
    //     description: description,
    //     objectId: uuid,
    // })
    //     // .then(function(docRef) {
    //     //     console.log("Document written with ID: ", docRef.id);
    //     // })
    //     .catch(function(error) {
    //         console.error("Error adding document: ", error);
    //     });
}

function updateUserInDbWithProject() {

    // db.collection('Users').doc(firebase.auth().currentUser.uid).update({
    //     projects: firebase.firestore.FieldValue.arrayUnion(uuid),
    // })
    //     // .then(function(docRef) {
    //     //     console.log("Document written with ID: ", docRef.id);
    //     // })
    //     .catch(function(error) {
    //         console.error("Error adding document: ", error);
    //     });
}

function emptyFields() {
    document.getElementById("exampleFormControlTextarea1").value = null;
    document.getElementById("name").value = null;
}

function presentApp() {

    let path = '/home/projects';
    window.location.href=path
    // console.log(firebase.auth().currentUser.uid);
}

function waitTime(name, description) {

    setTimeout(function(){
        addProjectToDb(name, description);
    }, 1000);
    setTimeout(function(){
        updateUserInDbWithProject();
    }, 1000);
    setTimeout(function(){
        emptyFields();
    }, 1000);
    setTimeout(function(){
        presentApp();
    }, 10000);

}

class CreateProject extends Component{

    render(){
    return (
        <div className="content">
            <Grid fluid>
                <Row>
                    <Col md={8}>
                        <Card
                            title="Add Project"
                            content={
                                <form>
                                    <FormInputs
                                        ncols={["col-md-6"]}
                                        properties={[
                                            {
                                                label: "Project Name",
                                                id: "name",
                                                type: "text",
                                                bsClass: "form-control",
                                                placeholder: "Name",
                                            },
                                        ]}
                                    />
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlTextarea1">
                                            Project Description
                                        </label>
                                        <textarea
                                            className="form-control"
                                            id="exampleFormControlTextarea1"
                                            rows="5"
                                            placeholder="Enter your description here..."
                                        />
                                    </div>

                                    <Button onClick={() => waitTime(getName(), getDescription())} bsStyle="info" pullRight fill>
                                        Create Project
                                    </Button>
                                    <div className="clearfix" />
                                </form>
                            }
                        />
                    </Col>
                    <Col md={4}>
                        <UserCard
                            name="Mike Andrew"
                            userName="michael24"
                            description={
                                <span>
                    "Project Manager/Normal User"
                  </span>
                            }
                            socials={
                                <div>
                                    <Button simple>
                                        <i className="fa fa-github-square fa-lg" />
                                    </Button>
                                </div>
                            }
                        />
                    </Col>
                </Row>
            </Grid>
        </div>
    );

}
}

export default CreateProject
