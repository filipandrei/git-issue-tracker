import React, { Component } from "react";
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import Axios from "axios"

import SignIn from "./SignIn";

const base_url = 'http://127.0.0.1:8000/';

let projectsArray = [[]], projects;

function getFirstName() {

    return localStorage.getItem('first_name');
}

function getLastName() {

    return localStorage.getItem('last_name');
}

function getUsername() {

    return localStorage.getItem('username');
}

function getUsernameField() {

    return document.getElementById("username").value;
}

function updateCurrentUserData(value) {
    Axios.patch(base_url + 'social/users/update/', {
        'user' : {
            'username' :  value,
        },

        'profile' : {
            'username' :  value,
        }
    })
        .then(response => {
            console.log(response)
            console.log(response.status + " " + response.statusText)
        })
        .catch(error => {
            console.log(error)
        })

}


class UserProfile extends Component {

    componentDidMount() {
        fetch(base_url + 'social/current_profile/', {
            method : 'GET',
            headers : {
                Authorization : `JWT ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(profile => {
                console.log("Mere ba pulaa!")
                console.log(profile);
            })
    }

    render() {

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card
                                title="Edit Profile"
                                content={
                                    <form>
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "Username",
                                                    type: "text",
                                                    id: "username",
                                                    bsClass: "form-control",
                                                    placeholder: getUsername(),
                                                    // value: '',
                                                },
                                                {
                                                    label: "Email address",
                                                    type: "email",
                                                    id: "email",
                                                    bsClass: "form-control",
                                                    placeholder: "Email",
                                                    value: '',
                                                    // value: getEmail(),
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "First name",
                                                    type: "text",
                                                    id: "firstName",
                                                    bsClass: "form-control",
                                                    placeholder: "First name",
                                                    value: getFirstName(),
                                                },
                                                {
                                                    label: "Last name",
                                                    type: "text",
                                                    id: "lastName",
                                                    bsClass: "form-control",
                                                    placeholder: "Last name",
                                                    value: getLastName(),
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-14"]}
                                            properties={[
                                                {
                                                    label: "GitHub Token",
                                                    type: "character",
                                                    bsClass: "form-control",
                                                    placeholder: "Paste you GitHub Token here..."
                                                }
                                            ]}
                                        />

                                        <FormInputs
                                            ncols={["col-md-14"]}
                                            properties={[
                                                {
                                                    label: "GitHub account",
                                                    type: "character",
                                                    bsClass: "form-control",
                                                    placeholder: "Place your GitHub user link..."
                                                }
                                            ]}
                                        />
                                        <Button onClick={() => updateCurrentUserData(getUsernameField())}
                                                style={{marginTop: 20, marginLeft: 20, _height: 30, _weigh: 40, bsSizes: 'large'}}
                                                pullRight>
                                            Update Profile
                                        </Button>
                                        <div className="clearfix"/>
                                    </form>
                                }
                            />
                        </Col>
                        <Col md={4}>
                            <UserCard
                                name="Filip Andrei"
                                userName="filipandrei"
                                description={
                                    <span>
                    "Project Manager/Normal User"
                  </span>
                                }
                                socials={
                                    <div>
                                        <Button simple onClick={() => {
                                            window.open("https://github.com")
                                        }}>
                                            <i className="fa fa-github-square fa-lg"/>
                                        </Button>
                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default UserProfile
