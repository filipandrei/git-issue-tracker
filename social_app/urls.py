from django.urls import path
from .views import *

urlpatterns = [
    path('current_user/', get_current_user),
    path('users/update/', UpdateUserView.as_view()),
    path('current_profile/', get_current_profile),
    path('users/create', CreateUserView.as_view()),
]
