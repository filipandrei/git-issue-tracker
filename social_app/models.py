from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User
# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = 'models.CharField(max_length=64)'
    # projects = ArrayField(models.CharField(max_length=128, blank=False), size=30)


# class Project(models.Model):
#     name = models.CharField(max_length=50)
#     created_by = models.ForeignKey(Profile, on_delete=models.CASCADE)
#     # members = models.ManyToManyField(Profile)
