from django.shortcuts import render
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin

from social_app.serializers import *
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions


@api_view(['GET'])
def get_current_user(request):
    serializer = GetFullUserSerializer(request.user)
    return Response(serializer.data)


@api_view(['GET'])
def get_current_profile(request):
    serializer = ProfileSerializer(request.user.profile)
    return Response(serializer.data)


class CreateUserView(APIView):
    permission_classes = (permissions.AllowAny, )

    def post(self, request):
        user = request.data.get('user')
        if not user:
            return Response({'response': 'error', 'message': 'No data found'})
        serializer = UserSerializerWithToken(data = user)
        if serializer.is_valid():
            saved_user = serializer.save()
        else:
            return Response({"response": "error", "message": serializer.errors})
        return Response({"response": "success", "message": "user created succesfully"})


class UpdateUserView(APIView):
    permission_classes = (permissions.AllowAny, )

    def patch(self, request):
        user = request.data.get('user')
        profile = request.data.get('profile')
        if not user:
            return Response({'response': 'error', 'message': 'No data found'})
        user_serializer = GetFullUserSerializer(data=user)
        profile_serializer = ProfileSerializer(data=profile)
        if user_serializer.is_valid() and profile_serializer.is_valid():
            saved_user = user_serializer.update()
            saved_profile = profile_serializer.update()
        else:
            return Response({"response": "error", "message": user_serializer.errors})
        return Response({"response": "success", "message": "user updated succesfully"})
