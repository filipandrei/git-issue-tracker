from rest_framework import serializers as sz
from rest_framework_jwt.serializers import User
from rest_framework_jwt.settings import api_settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Profile


class GetFullUserSerializer(sz.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser', 'first_name', 'last_name')


class UserSerializerWithToken(sz.ModelSerializer):
    password = sz.CharField(write_only=True)
    token = sz.SerializerMethodField()

    def get_token(self, object):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(object)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()

        # ProfileSerializer.create_user_profile(User, user, True)
        ProfileSerializer.save_user_profile(User, user)
        # print(user.profile.email)
        return user

    class Meta:
        model = User
        fields = ('token', 'username', 'password', 'first_name',
                  'last_name')


class ProfileSerializer(sz.ModelSerializer):

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    class Meta:
        model = Profile
        # fields = "__all__"
        fields = ('user', 'email')

